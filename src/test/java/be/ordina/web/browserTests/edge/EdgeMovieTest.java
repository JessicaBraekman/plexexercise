package be.ordina.web.browserTests.edge;

import be.ordina.web.BaseTest;
import be.ordina.web.utils.AllureUtils;
import be.ordina.web.utils.SeleniumUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;

import java.io.IOException;

import static java.lang.System.setProperty;
import static org.junit.Assert.assertTrue;

public class EdgeMovieTest {

    private BaseTest baseTest;
    private WebDriver driver;

    private String email;
    private String password = "PlexTest!";
    private String movie = "Aspe";


    @Before
    public void setUp(){
        setProperty("webdriver.edge.driver", "src/test/resources/browsers/msedgedriver.exe");
        driver = new EdgeDriver();
        driver.get("https://www.plex.tv/nl/");
        driver.manage().window().maximize();

        baseTest = new BaseTest(driver);
    }

    @After
    public void tearDown(){
        driver.quit();
    }

    @Test
    public void successfulStreaming() throws IOException {
        email = "PlexTester1@mailinator.com";
        baseTest.login(email, password);
        baseTest.playVideo();

        String message = baseTest.MovieIsPlaying();
        assertTrue(message.contains("Pause"));

        SeleniumUtils.screenshot(driver, "Edge_SuccesFullStreaming");
        AllureUtils.screenshot(driver, "Edge_SuccesFullStreaming");
    }

    @Test
    public void successfullSearch() throws IOException {
        email = "PlexTester2@mailinator.com";
        baseTest.login(email, password);
        baseTest.searchAMovie(movie);

        String title = baseTest.searchFound();
        assertTrue(title.contains(movie));

        SeleniumUtils.screenshot(driver, "Edge_SuccessFullSearch");
        AllureUtils.screenshot(driver, "Edge_SuccessFullSearch");
    }

    @Test
    public void successfullAddToWatchlist() throws IOException {
        email = "PlexTester1@mailinator.com";
        baseTest.login(email, password);
        baseTest.searchAMovie(movie);

        baseTest.addOrRemoveMovieWatchList();

        String addedToWatchList = baseTest.checkIfAddedOrRemovedFromWatchList();
        assertTrue(addedToWatchList.contains("preplay-removeFromWatchlist"));

        SeleniumUtils.screenshot(driver, "Edge_SuccessFullAddToWatchList");
        AllureUtils.screenshot(driver, "Edge_SuccessFullAddToWatchList");

        baseTest.addOrRemoveMovieWatchList();

        String RemovedFromWatchList = baseTest.checkIfAddedOrRemovedFromWatchList();
        assertTrue(RemovedFromWatchList.contains("preplay-addToWatchlist"));

        SeleniumUtils.screenshot(driver, "Edge_SuccessFullRemovedFromWatchList");
        AllureUtils.screenshot(driver, "Edge_SuccessFullRemovedFromWatchList");
    }

    @Test
    public void successfullMarkAndUnMarkedAsPlayed() throws IOException {
        email = "PlexTester33@mailinator.com";
        baseTest.login(email, password);
        baseTest.searchAMovie(movie);
        baseTest.markAMovieAsPlayed();

        String checkAddedToWatchList = baseTest.checkIfMarkedAsPlayed();
        assertTrue(checkAddedToWatchList.contains("plex-icon-toolbar-mark-unplayed-560"));

        SeleniumUtils.screenshot(driver, "Edge_SuccessFullMarkAsPlayed");
        AllureUtils.screenshot(driver, "Edge_SuccessFullMarkAsPlayed");

        baseTest.unMarkAMovieAsPlayed();

        String checkRemovedToWatchList = baseTest.checkUnMarkedAsPlayed();
        assertTrue(checkRemovedToWatchList.contains("plex-icon-toolbar-mark-played-560"));

        SeleniumUtils.screenshot(driver, "Edge_SuccessFullUnMarkAsPlayed");
        AllureUtils.screenshot(driver, "Edge_SuccessFullUnMarkAsPlayed");
    }
}
