package be.ordina.web.browserTests.edge;

import be.ordina.web.BaseTest;
import be.ordina.web.utils.AllureUtils;
import be.ordina.web.utils.SeleniumUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;

import java.io.IOException;

import static java.lang.System.setProperty;
import static org.junit.Assert.assertTrue;

public class EdgeAccountTest {

    private BaseTest baseTest;
    private WebDriver driver;

    private String email;
    private String password = "PlexTest!";

    @Before
    public void setUp(){
        setProperty("webdriver.edge.driver", "src/test/resources/browsers/msedgedriver.exe");
        driver = new EdgeDriver();
        driver.get("https://www.plex.tv/nl/");
        driver.manage().window().maximize();

        baseTest = new BaseTest(driver);
    }

    @After
    public void tearDown(){
       driver.quit();
    }


    @Test
    public void SuccessFullToAccountPage() throws IOException {
        email = "PlexTester1@mailinator.com";
        baseTest.login(email, password);
        baseTest.goToAccount();

        String message = baseTest.getSuccesOnAccountPage();
        assertTrue(message.contains("Account"));

        SeleniumUtils.screenshot(driver, "Chrome_SuccessFullOnAccountPage");
        AllureUtils.screenshot(driver, "Chrome_SuccessFullOnAccountPage");
    }

    @Test
    public void SuccessFullChangeUserName() throws IOException {
        email = "PlexTester2@mailinator.com";
        baseTest.login(email, password);
        baseTest.goToAccount();

        String newUserName = baseTest.SuccessFullChangeUserName();
        String message = baseTest.getSuccessFullChangeUserName();
        System.out.println(message);
        assertTrue(message.contains(newUserName));

        SeleniumUtils.screenshot(driver, "Chrome_UnSuccessUserNameChange");
        AllureUtils.screenshot(driver, "Chrome_UnSuccessUserNameChange");
    }

    @Test
    public void UnSuccessFullChangeUserName() throws IOException {
        email = "PlexTester33@mailinator.com";
        baseTest.login(email, password);
        baseTest.goToAccount();
        baseTest.UnSuccessFullChangeUserName();

        String message = baseTest.getUnSuccessFullChangeUserName();
        System.out.println(message);
        assertTrue(message.contains("USERNAME — USERNAME MUST NOT BE AN EMAIL ADDRESS"));

       SeleniumUtils.screenshot(driver, "Chrome_UnSuccessUserNameChange");
        AllureUtils.screenshot(driver, "Chrome_UnSuccessUserNameChange");
    }
}
