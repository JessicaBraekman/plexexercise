package be.ordina.web.browserTests.edge;

import be.ordina.web.BaseTest;
import be.ordina.web.utils.AllureUtils;
import be.ordina.web.utils.SeleniumUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;

import java.io.IOException;

import static java.lang.System.setProperty;
import static org.junit.Assert.assertTrue;

public class EdgeLoginTest {

    private BaseTest baseTest;
    private WebDriver driver;

    private String email;
    private String password = "PlexTest!";


    @Before
    public void setUp(){
        setProperty("webdriver.edge.driver", "src/test/resources/browsers/msedgedriver.exe");
        driver = new EdgeDriver();
        driver.get("https://www.plex.tv/nl/");
        driver.manage().window().maximize();

        baseTest = new BaseTest(driver);
    }

    @After
    public void tearDown(){
        driver.quit();
    }


    @Test
    public void SuccessFullLogin() throws IOException {
        email = "PlexTester1@mailinator.com";;
        baseTest.login(email, password);

        String message = baseTest.getSuccessLoginMessage();
        assertTrue(message.contains("Starten"));

        SeleniumUtils.screenshot(driver, "Chrome_SuccessFullLogin");
        AllureUtils.screenshot(driver, "Chrome_SuccessFullLogin");
    }

    @Test
    public void unSuccessFullLogin() throws IOException {
        email = "test@test.be";
        baseTest.login(email, password);

        String message = baseTest.getUnSuccessRegistrationMessage();
        assertTrue(message.contains("De gebruikersnaam of wachtwoord is onjuist"));

        SeleniumUtils.screenshot(driver, "Chrome_UnSuccessFullLogin");
        AllureUtils.screenshot(driver, "Chrome_UnSuccessFullLogin");
    }
}
