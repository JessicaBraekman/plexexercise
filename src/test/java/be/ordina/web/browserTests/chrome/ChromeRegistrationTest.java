package be.ordina.web.browserTests.chrome;

import be.ordina.web.BaseTest;
import be.ordina.web.utils.AllureUtils;
import be.ordina.web.utils.SeleniumUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;

import static java.lang.System.setProperty;
import static org.junit.Assert.assertTrue;

public class ChromeRegistrationTest {

    private BaseTest baseTest;
    private WebDriver driver;
    private WebDriverWait wait;

    private String email;
    private String password = "PlexTest!";
    private String movie = "Aspe";


    @Before
    public void setUp(){
        setProperty("webdriver.chrome.driver", "src/test/resources/browsers/chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("https://www.plex.tv/nl/");
        driver.manage().window().maximize();

       baseTest = new BaseTest(driver);
    }

    @After
    public void tearDown(){
        //driver.close();
        driver.quit();
    }

    @Test
    public void successfulRegistrationTest() throws InterruptedException, IOException {
        email =baseTest.getRandomEmail()+"@mailinator.com";
        baseTest.registration(email, password);

        String message = baseTest.getSuccessRegistrationMessage();
        System.out.println(message);

        assertTrue(message.contains("Home"));

        SeleniumUtils.screenshot(driver, "Chrome_SuccessfulRegistration");
        AllureUtils.screenshot(driver, "Chrome_UnSuccessFullRegistration");
    }

    @Test
    public void unSuccessfulRegistration() throws IOException {
        email = "test@test.com";
        baseTest.registration(email, password);

        String message = baseTest.getUnSuccessRegistrationMessage();
        assertTrue(message.contains("E-mail is al in gebruik"));

        SeleniumUtils.screenshot(driver, "Chrome_UnSuccessFulRegistration");
        AllureUtils.screenshot(driver, "Chrome_UnSuccessFullRegistration");
    }
}
