package be.ordina.web.browserTests.fireFox;

import be.ordina.web.BaseTest;
import be.ordina.web.utils.AllureUtils;
import be.ordina.web.utils.SeleniumUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.IOException;

import static java.lang.System.setProperty;
import static org.junit.Assert.assertTrue;

public class FireFoxMovieTest {

    private BaseTest baseTest;
    private WebDriver driver;

    private String email;
    private String password = "PlexTest!";
    private String movie = "Aspe";


    @Before
    public void setUp(){
        setProperty("webdriver.gecko.driver", "src/test/resources/browsers/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.get("https://www.plex.tv/nl/");
        driver.manage().window().maximize();

        baseTest = new BaseTest(driver);
    }

    @After
    public void tearDown(){
        driver.quit();
    }

    @Test
    public void successfulStreaming() throws IOException {
        email = "PlexTester1@mailinator.com";
        baseTest.login(email, password);
        baseTest.playVideo();

        String message = baseTest.MovieIsPlaying();
        assertTrue(message.contains("Pause"));

        SeleniumUtils.screenshot(driver, "FireFox_SuccesFullStreaming");
        AllureUtils.screenshot(driver, "FireFox_SuccesFullStreaming");
    }

    @Test
    public void successfullSearch() throws IOException {
        email = "PlexTester2@mailinator.com";
        baseTest.login(email, password);
        baseTest.searchAMovie(movie);

        String title = baseTest.searchFound();
        assertTrue(title.contains(movie));

        SeleniumUtils.screenshot(driver, "FireFox_SuccessFullSearch");
        AllureUtils.screenshot(driver, "FireFox_SuccessFullSearch");
    }

    @Test
    public void successfullAddToWatchlist() throws IOException {
        email = "PlexTester1@mailinator.com";
        baseTest.login(email, password);
        baseTest.searchAMovie(movie);

        baseTest.addOrRemoveMovieWatchList();

        String addedToWatchList = baseTest.checkIfAddedOrRemovedFromWatchList();
        assertTrue(addedToWatchList.contains("preplay-removeFromWatchlist"));

        SeleniumUtils.screenshot(driver, "FireFox_SuccessFullAddToWatchList");
        AllureUtils.screenshot(driver, "FireFox_SuccessFullAddToWatchList");

        baseTest.addOrRemoveMovieWatchList();

        String RemovedFromWatchList = baseTest.checkIfAddedOrRemovedFromWatchList();
        assertTrue(RemovedFromWatchList.contains("preplay-addToWatchlist"));

        SeleniumUtils.screenshot(driver, "FireFox_SuccessFullRemovedFromWatchList");
        AllureUtils.screenshot(driver, "FireFox_SuccessFullRemovedFromWatchList");
    }

    @Test
    public void successfullMarkAndUnMarkedAsPlayed() throws IOException {
        email = "PlexTester33@mailinator.com";
        baseTest.login(email, password);
        baseTest.searchAMovie(movie);
        baseTest.markAMovieAsPlayed();

        String checkAddedToWatchList = baseTest.checkIfMarkedAsPlayed();
        assertTrue(checkAddedToWatchList.contains("plex-icon-toolbar-mark-unplayed-560"));

        SeleniumUtils.screenshot(driver, "FireFox_SuccessFullMarkAsPlayed");
        AllureUtils.screenshot(driver, "FireFox_SuccessFullMarkAsPlayed");

        baseTest.unMarkAMovieAsPlayed();

        String checkRemovedToWatchList = baseTest.checkUnMarkedAsPlayed();
        assertTrue(checkRemovedToWatchList.contains("plex-icon-toolbar-mark-played-560"));

        SeleniumUtils.screenshot(driver, "FireFox_SuccessFullUnMarkAsPlayed");
        AllureUtils.screenshot(driver, "FireFox_SuccessFullUnMarkAsPlayed");
    }
}
