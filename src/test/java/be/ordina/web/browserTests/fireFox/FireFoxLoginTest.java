package be.ordina.web.browserTests.fireFox;

import be.ordina.web.BaseTest;
import be.ordina.web.utils.AllureUtils;
import be.ordina.web.utils.SeleniumUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;

import static java.lang.System.setProperty;
import static org.junit.Assert.assertTrue;

public class FireFoxLoginTest {

    private BaseTest baseTest;
    private WebDriver driver;
    private WebDriverWait wait;

    private String email;
    private String password = "PlexTest!";
    private String movie = "Aspe";


    @Before
    public void setUp(){
        setProperty("webdriver.gecko.driver", "src/test/resources/browsers/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.get("https://www.plex.tv/nl/");
        driver.manage().window().maximize();

       baseTest = new BaseTest(driver);
    }

    @After
    public void tearDown(){
        driver.quit();
    }


    @Test
    public void SuccessFullLogin() throws IOException {
        email = "PlexTester1@mailinator.com";;
        baseTest.login(email, password);

        String message = baseTest.getSuccessLoginMessage();
        System.out.println(message);
        assertTrue(message.equals("Starten"));

        SeleniumUtils.screenshot(driver, "FireFox_SuccessFullLogin");
        AllureUtils.screenshot(driver, "FireFox_SuccessFullLogin");
    }

    @Test
    public void unSuccessFullLogin() throws IOException {
        email = "test@test.be";
        baseTest.login(email, password);

        String message = baseTest.getUnSuccessRegistrationMessage();
        System.out.println(message);
        assertTrue(message.contains("De gebruikersnaam of wachtwoord is onjuist"));

        SeleniumUtils.screenshot(driver, "Chrome_UnSuccessFullLogin");
        AllureUtils.screenshot(driver, "Chrome_UnSuccessFullLogin");
    }
}
