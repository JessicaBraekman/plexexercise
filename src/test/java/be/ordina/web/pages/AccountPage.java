package be.ordina.web.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AccountPage {

    private final WebDriver driver;
    private WebDriverWait wait;

    //Account
    private By titleAccount =By.xpath("//*[@id=\"content\"]/div/div/div[2]/div/h2");

    private By btnChangeUserName = By.xpath("//*[@id=\"content\"]/div/div/div[2]/div/div[5]/div/div[1]/button");
    private By tbUserName = By.xpath("//*[@id='username']");

    private By erChangeUserName = By.xpath("//*[@id=\"content\"]/div/div/div[2]/div/div[5]/div[1]");
    private By lblNewUserName = By.xpath("//*[@id=\"content\"]/div/div/div[2]/div/div[5]/div/div[2]");

    public AccountPage(WebDriver chromeDriver){
        this.driver = chromeDriver;
        this.wait = new WebDriverWait(chromeDriver, 10);
    }

    public String getSuccessFullLandedOnAccountPage(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(titleAccount));
        String message = driver.findElement(titleAccount).getText();
        return message;
    }

    public void changeUserName(String userName){
        wait.until(ExpectedConditions.visibilityOfElementLocated(btnChangeUserName));
        driver.findElement(btnChangeUserName).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(tbUserName));
        WebElement elUserName = driver.findElement(tbUserName);
        elUserName.clear();
        elUserName.sendKeys(userName);
        elUserName.submit();
    }

    public String getUnSuccessFullChangedUserName(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(erChangeUserName));
        String message = driver.findElement(erChangeUserName).getText();
        return message;
    }

    public String getSuccessFullChangedUserName() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(lblNewUserName));
        String message = driver.findElement(lblNewUserName).getText();
        return message;
    }
}
