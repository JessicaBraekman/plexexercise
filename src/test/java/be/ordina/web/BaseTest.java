package be.ordina.web;

import be.ordina.web.pages.AccountPage;
import be.ordina.web.pages.HomePage;
import be.ordina.web.pages.MoviePage;
import be.ordina.web.pages.RegistrationAndLoginModal;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class BaseTest {

    private final WebDriver driver;
    private WebDriverWait wait;

    private HomePage homePage;
    private RegistrationAndLoginModal registrationAndLoginModal;
    private MoviePage moviePage;
    private AccountPage accountPage;

    public BaseTest(WebDriver driver){
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 10);

        homePage = new HomePage(driver);
        registrationAndLoginModal = new RegistrationAndLoginModal(driver);
        moviePage = new MoviePage(driver);
        accountPage = new AccountPage(driver);
    }
    /**
     * getRanomEmail
     * Generates an random e-email string for the registation
     * @return random string
     */
    public String getRandomEmail(){
        // create a string of uppercase and lowercase characters and numbers
        String upperAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String lowerAlphabet = "abcdefghijklmnopqrstuvwxyz";
        String numbers = "0123456789";

        // combine all strings
        String alphaNumeric = upperAlphabet + lowerAlphabet + numbers;

        // create random string builder
        StringBuilder sb = new StringBuilder();

        // create an object of Random class
        Random random = new Random();

        // specify length of random string
        int length = 10;

        for(int i = 0; i < length; i++) {

            // generate random index number
            int index = random.nextInt(alphaNumeric.length());

            // get character specified by index
            // from the string
            char randomChar = alphaNumeric.charAt(index);

            // append the character to string builder
            sb.append(randomChar);
        }

        String randomString = sb.toString();

        return  randomString;
    }

    /**
     * registration
     * @param driver gives the correct driver
     * @param homePage
     * @param registrationAndLoginModal
     * @param email gives the email address that needs to be used
     * @param password gives the password that needs to be used
     *
     * Registrates a user with given email and password
     */
    public void registration(String email, String password){
       // driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
       // homePage.clickAccept();

        homePage.clickSignUp();
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        driver.switchTo().frame("fedauth-iFrame");

        registrationAndLoginModal.registrationOrLogin(email, password);
    }

    /*
     * Get 'HOME' after user is logged in
     */
    public String getSuccessRegistrationMessage(){
        return homePage.getSuccessRegistrationMessage();
    }

    /*
     * Get an error message
     */
    public String getUnSuccessRegistrationMessage()
    {
        return homePage.getUnSuccessRegistrationMessage();
    }

    /**
     * Login
     * @param email
     * @param password
     */
    public void login(String email, String password){
       // driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
       // homePage.clickAccept();

        homePage.clickSignIn();
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        driver.switchTo().frame("fedauth-iFrame");

        registrationAndLoginModal.registrationOrLogin(email, password);
    }

    public String getSuccessLoginMessage(){
        driver.switchTo().defaultContent();
        return registrationAndLoginModal.getSuccessLoginMessage();
    }

    /**
     * PlayVideo
     */
    public void playVideo(){
        registrationAndLoginModal.goToHome();
        moviePage.playVideo();
    }

    public String MovieIsPlaying(){
        return moviePage.MovieIsPlaying();
    }

    /**
     * Search A Movie
     */
    public void searchAMovie(String movie){
        registrationAndLoginModal.goToHome();
        moviePage.searchVideo(movie);
        moviePage.selectSearchResult();
    }
    public String searchFound(){
        return moviePage.searchFound();
    }

    /**
     * Add to WatchList
     */
    public void addOrRemoveMovieWatchList(){
        moviePage.addMovieToWatchList();
    }
    public String checkIfAddedOrRemovedFromWatchList() {
        return moviePage.checkIfAddedOrRemovedFromWatchList();
    }

    /**
     * Mark as Played
     */
    public void markAMovieAsPlayed(){
        moviePage.markAsPlayed();
    }
    public String checkIfMarkedAsPlayed(){
        return moviePage.checkIfMarkedAsPlayed();
    }

    /**
     * Mark as UnPlayed
     */
    public void unMarkAMovieAsPlayed(){
        moviePage.unMarkAsPlayed();
    }
    public String checkUnMarkedAsPlayed(){
        return moviePage.checkIfMarkedAsPlayed();
    }

    /**
     * Go To AccountPage
     */
    public void goToAccount(){
        registrationAndLoginModal.goToHome();
        moviePage.goToAccount();
    }

    public String getSuccesOnAccountPage(){
       return accountPage.getSuccessFullLandedOnAccountPage();
    }

    public void UnSuccessFullChangeUserName(){
        accountPage.changeUserName(getRandomEmail()+"@mailinator.com");
    }

    public String getUnSuccessFullChangeUserName(){
        return accountPage.getUnSuccessFullChangedUserName();
    }

    public String SuccessFullChangeUserName() {
     String newUserName = getRandomEmail();
     accountPage.changeUserName(newUserName);
     return newUserName;
    }

    public String getSuccessFullChangeUserName() {
        return accountPage.getSuccessFullChangedUserName();
    }
}
